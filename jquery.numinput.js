/**
* jQuery NumInput 1.1
*
* Copyright (c) 2013 Belukov Alexey (belukov@gmail.com)
* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
*/

/**
*	DESCRIPTION:
*	Plugin take the input object, plus and minus button
*	allow incrace or decrace input value by click on the buttons
*	check and filter value if it typed manually
*
*	SOURCE:
*	https://bitbucket.org/belukov/jquery-numinput
*	
*	USAGE:
*	$('input#my_input').numinput({
*		minus: 'a#my_minus_btn', // allow css selector
*		plus: $('a#my_plus_btn'), // or the jquery object
*		min: 1, // default = 0; numeric value or false for disable minimum
*		max: 100 // default = false; numeric or false;
*		});
*	$('input#my_input').numinput('plus'); // +1
*	$('input#my_input').numinput('minus'); // -1
*	$('input#my_input').numinput('set', +100500); // = 100500
*	$('input#my_input').numinput('append', 5); // +5
*	var value =  $('input#my_input').numinput('get'); // get current value
*	
*	TODO:
*	- check type of init object
*	- check min and max values
*	- add "step" option
*	- add float support
*
*/

(function( $ ){ 

	$.fn.numinput = function(method)
	{


		var objects = {
			input: null,
			plus: null,
			minus: null
		}

		var default_options = {
			min: 0,
			max: false
		}
		var options = {}

		var methods = {
			init: function(_options) // {{{
			{
			
				console.log('numinput init with options:');
				console.log(_options);

				options = $.extend(default_options, _options);

				objects.input = $(this);
				// TODO: check for imput['type="text"']

				if(options.minus){
					var _obj = $(options.minus);
					if(_obj.length != 1){
						console.error("can't init minus btn");
					}else{
						objects.minus = _obj;
						objects.minus.bind('click', function(e)
						{
							e.preventDefault();
							methods.minus();
						});
					}
				}
				if(options.plus){
					var _obj = $(options.plus);
					if(_obj.length != 1){
						console.error("can't init plus btn");
					}else{
						objects.plus = _obj;
						objects.plus.bind('click', function(e)
						{
							e.preventDefault();
							methods.plus();
						});
					}
				}
				methods.set(methods.get()); // filter value

				objects.input.bind('change', function()
				{
					methods.set( $(this).val() );
				})

			} // }}}  ~init 
			
			,minus: function() // {{{
			{
				return methods.append(-1);
			} // }}} ~minus


			,plus: function() // {{{
			{
				return methods.append(1);
			} // }}} ~plus

			,get: function() // {{{
			{
				var val = parseInt(objects.input.val());
				return val;
			} //}}}

			,set: function(val) // {{{
			{
				val = parseInt(val);
				if(isNaN(val)) val = 0;
				if(false !== options.min && val < options.min) val = options.min;
				if(false !== options.max && val > options.max) val = options.max;
				objects.input.val(val);
				return $(this);
			} // }}}

			,append: function(add) // {{{
			{
				var val = methods.get();
				add = parseInt(add);
				if(isNaN(add)) add = 0;
				return methods.set( val + add );
				
			} // }}}
		}

	
		console.log("call jq.numinput with: ");
		console.log(method);

		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			console.error( 'method "' + method + '" not exist in jQuery.numinput' );
		}

	};

})( jQuery );
