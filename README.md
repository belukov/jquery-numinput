DESCRIPTION
=============

Plugin take the input object, plus and minus button.
Аllow incrace or decrace input value by click on the buttons.
Сheck and filter value if it typed manually.

USAGE
--------

```
<script>
$('input#my_input').numinput({
	minus: 'a#my_minus_btn', // allow css selector
	plus: $('a#my_plus_btn'), // or the jquery object
	min: 1, // default = 0; numeric value or false for disable minimum
	max: 100 // default = false; numeric or false;
	});
$('input#my_input').numinput('plus'); // +1
$('input#my_input').numinput('minus'); // -1
$('input#my_input').numinput('set', +100500); // = 100500
$('input#my_input').numinput('append', 5); // +5
var value =  $('input#my_input').numinput('get'); // get current value
</script>
```

TODO
--------
* check type of init object
* check min and max values
* add "step" option
* add float support